/*
	Selection Control Structures
		-sorts out whether the statement/s are to be executed based on the condition whether it is true or false

		1. If Else statement
		2. switch statement
		3. Try catch finally statement

		
		-If-Else statement

		Sytanx: if(condition) {
			statement
		} else {
			statement
		}

*/

// If-Statement
// -executes a statement if a specified condition is true
// -can stand alone even without the else statement

/*

	Syntax:
		if(condition) {
			statement
		} else {
			statement
		}
*/

let numA = -1;
// < - less than
// > - greater than
// <= - less than or equal
// >= - greater than or equal

// if statement
if(numA < 0) {
	console.log("Hello");
}

console.log(numA < 0);

// else if
/*
	- Executes a statement if previous condition are false and if the specified condition is true.
	- The "else if" clause is optional and can be added to capture additional conditions to change the flow of a program.

*/

let numB = 1;

if(numA > 0) {
	console.log("Hello")
} else if (numB > 0) {
	console.log("World")
}

// We were able to run the else if statement after we evaluated that the if condition was failed.
// If the if() condition was passed and run, we will no longer evaluate the else if() and end the process there

city = "Tokyo";

if(city === "New York") {
	console.log("Welcome to New York City");
} else if (city === "Tokyo") {
	console.log("Welcome to Tokyo");
}

// Else statement
/*
	-Executes a statement if all other conditions are false
	-The "else" statement is optional and can be added to capture any other result to change the flow of a program

*/

if(numA > 0){
	console.log("Hello");
} else if (numB === 0) {
	console.log("World");
} else {
	console.log("Again");
}

let age = 20;

if (age <= 17) {
	console.log("Not allowed to drink!");
} else {
	console.log("Matanda ka na, shot na!");
}


function getHeightRequirement () {
	let height = 143;

	if (height < 150) {
	console.log("Did not passed min height req.");
	} else if (height >= 150){
	console.log("Passed the minimum height req");
	}

}

getHeightRequirement();

// if, else if and else statements with functions

let message = "No message.";
console.log(message);

function determineTyphoonIntensity(windSpeed) {

	if(windSpeed < 30) {
		return 'Not a typhoon yet.'
	} else if (windSpeed <= 61) {
		return 'Tropical depression detected.'
	} else if (windSpeed >= 62 && windSpeed <= 88) {
		return 'Tropical storm detected.'
	} else if (windSpeed >= 89 && windSpeed <= 117) {
		return 'Severe tropical storm detected'
	} else {
		return 'Typhoon detected.'
	}
}

// Return the string to the variable "message" that invoked it
message = determineTyphoonIntensity(70);
console.log(message);

if (message === "Tropical storm detected.") {
	console.warn(message);
	console.error(message);
}

// Truthy and Falsy
/*
	-In JavaScript a "truthy" value is a value that is considered TRUE when encountered in a Boolean context
	-Values are considered true unless defined otherwise:
	-Falsy values/exception for Truthy
	1. false
	2. 0
	3. -0
	4. ""
	5. null
	6. undefined
	7. NaN

*/

// Truthy Examples
if(true) {
	console.log("Truthy");
}

if(1) {
	console.log("Truthy");
}

if([]) {
	console.log("Truthy");
}

// Falsy Examples
if(false) {
	console.log("Falsy");
}

if(0) {
	console.log("Falsy");
}

if(undefined) {
	console.log("Falsy");
}

// Conditional (Ternary) Operator
/*
	Syntax:
		(expression) ? ifTrue : ifFalse;

*/

// Single Statement Execution
// Ternary Operators have an implicit "return" statement, meaning that without the "return" keyword, the resulting expression can be stored in a variable 
let ternaryResult = (1 < 18) ? "Statement is true" : "Statement is false";
console.log("Result of Ternary Operator: " + ternaryResult);


// Multiple Statement Execution
// let name = prompt("Enter your name: ");

// function isOfLegalAge() {

// 	return "You are of the legal age."
// }

// function isUnderAge(){

// 	return "You are under the age limit."
// }

// let age1 = parseInt(prompt("What is your age?"));
// console.log(age1);

// let legalAge = (age1 > 18) ? isOfLegalAge() : isUnderAge();
// console.log("Result of Ternary Operator in functions: " + legalAge + ", " + name);

// Switch Statement
/*
	Syntax:
		switch(expression) {
			case value:
				statement;
				break;
			default:
				statement;
				break
		}

*/

// let day = prompt("What day of the week is it today?").toLowerCase();
// console.log(day);

// switch(day) {

// 	case 'monday':
// 	case 'lunes':
// 		console.log("The color of the day is red.");
// 			break;
// 	case 'tuesday':
// 		console.log("The color of the day is orange.");
// 			break;
// 	case 'wednesday':
// 		console.log("The color of the day is yellow.");
// 			break;
// 	case 'thursday':
// 		console.log("The color of the day is green.");
// 			break;
// 	case 'friday':
// 		console.log("The color of the day is blue.");
// 			break;
// 	case 'saturday':
// 		console.log("The color of the day is indigo.");
// 			break;
// 	case 'sunday':
// 		console.log("The color of the day is violet.");
// 			break;
// 	default:
// 		console.log("Please input a valid day.");
// 			break;


// }

// Try Catch Finally Statement
// try catch statements are commonly used for ERROR handling 
function showIntensityAlert(windSpeed) {

	try{

		// Attempting to execute a code
		alerat(determineTyphoonIntensity(windSpeed))
	}

	catch(error) {

		// Catch errors within "try" statement
		console.log(error);
		console.log(error.message);
	}

	finally{
		// Continue execution of code regardless of success and failure of code execution in the "try" block to handle/resolve errors
		alert("Intensity updates will show new alert.");
	}

}

showIntensityAlert(56);