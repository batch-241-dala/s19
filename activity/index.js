let username;
let password;
let role;

function login() {
	username = prompt("Enter your username: ").toLowerCase();
	password = prompt("Enter your password: ").toLowerCase();
	role = prompt("Enter your role: ").toLowerCase();

	if (username === "" || username === " " || username === null) {
		alert("Please enter a valid username!");
	} else if (password === "" || password === " " || password === null) {
		alert("Please enter a valid password");
	} else if (role === ""  || role === " "  || role === null) {
		alert("Please enter a valid role")
	} else {
		switch(role) {
			case "admin":
				alert("Welcome back to the class portal, admin!");
				break;

			case "teacher":
				alert("Thank you for loggin in, teacher");
				break;

			case "student":
				alert("Welcome to the class portal, student!");
				break;

			default:
				alert("Role out of range")
				break;
		}
	}
}

login();

function checkAverage(num1, num2, num3, num4) {
	let average = Math.round((num1 + num2 + num3 + num4)/4);

	if (average <= 74) {
		console.log("Hello, student, your average is " + average + ". " + "The letter equivalent is F");
	} else if (average <= 79 && average >= 75) {
		console.log("Hello, student, your average is " + average + ". " + "The letter equivalent is D");
	} else if (average <= 84 && average >= 80 ) {
		console.log("Hello, student, your average is " + average + "." + "The letter equivalent is C");
	} else if (average <= 89 && average >= 85 ) {
		console.log("Hello, student, your average is " + average + ". " + "The letter equivalent is B");
	} else if (average <= 95 && average >= 90 ) {
		console.log("Hello, student, your average is " + average + ". " + "The letter equivalent is A");
	} else if (average > 96) {
		console.log("Hello, student, your average is " + average + ". " + "The letter equivalent is A+");
	}
}

checkAverage();